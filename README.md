### Demo Project: ###
- Run Nexus on Droplet and Publish Artifact to Nexus
### Technologies used: ###
- Nexus, DigitalOcean, Linux, Java, Gradle, Maven
### Project Description: ###
1. Install and configure Nexus from scratch on a cloud server
- create user adduser nexus
- set firewall and ssh key
- update apt repository and install java 8
- apt install net-tools
- downloads Nexus in **/opt** directory
- wget https://download.sonatype.com/nexus/3/nexus-3.56.0-01-unix.tar.gz 
- tar -zvxf nexus-3.56.0-01-unix.tar.gz
- **change ownership of nexus folders**
- chown \-R nexus:nexus sonatype-work
- chown \-R nexus:nexus nexus-3.56.0-01 tar file.
- vim nexus-3.56.0-01/bin/nexus.rc \- set run_as_user="nexus"
- su \- nexus
- /opt/nexus-3.56.0-01/bin/nexus start
- ps aux | grep nexus
- netstat \-lnpt
- configure nexus running port on firewall to login to nexus UI
- cat /opt/sonatype-work/nexus3/admin.password
2. Create new User on Nexus with relevant permissions
- create a user. 
- create a role for the user none admin
3. Java Gradle Project: Build Jar & Upload to Nexus
- configure maven plugin
- publication: artifact to upload
- respository: where nexus is running
- configure nexus credentials: set them in gradle.properties
- **build artifact and upload to Nexus**
- gradle build
- gradle publish
4. Java Maven Project: Build Jar & Upload to Nexus
- **In pom.xml**
- configure maven plugin 
- configure snapshot repository
- configure credentials in **.m2** using **settings.xml** file
- **build artifact and upload to Nexus**
- mvn package
- mvn deploy

#### Module 6: Artifact Repository Manager with Nexus ####